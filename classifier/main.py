#!/usr/bin/env python

import arff
import sys
import os
import pickle
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier

# from sklearn.tree import export_graphviz


TEST_SET_SIZE = 0.2
HIDDEN_LAYER_SIZES = (5,)
MODEL_SAVE_FILE = 'phishing_classifier.pkl'


models = {
    # TODO: Explore different customizable attributes of MLPClassifier, hidden layers etc.
    # http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html

    "neural network": MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=HIDDEN_LAYER_SIZES, random_state=1),
    "decision tree": DecisionTreeClassifier(),
    "perceptron": Perceptron(),
    "logistic regression": LogisticRegression(),
    "svm": SVC(),
    "naive bayes": GaussianNB(),
    "knn": KNeighborsClassifier(),
}

allowed_features = ["having_IP_Address",
                    "URL_Length",
                    "having_At_Symbol",
                    "Prefix_Suffix",
                    "having_Sub_Domain",
                    "SSLfinal_State",
                    "Request_URL",
                    "URL_of_Anchor",
                    "SFH",
                    "Abnormal_URL",
                    "Redirect",
                    "on_mouseover",
                    "RightClick",
                    "popUpWidnow",
                    "age_of_domain",
                    "DNSRecord",
                    "web_traffic",
                    "Result"]


def use_17_features(data, attributes):
    assert(len(attributes) == data.shape[1])
    # print(data[:, [1]])

    allowed = []
    allowed_attributes = []
    for i in range(len(attributes)):
        if attributes[i][0] in allowed_features:
            allowed.append(i)
            allowed_attributes.append(attributes[i][0])

    data = data[:, allowed]

    return data, allowed_attributes


def load_dataset(dataset_name):

    if not os.path.isfile(dataset_name):
        print("Error! The provided input file %s was not found." % dataset_name)
        sys.exit(1)

    dataset = arff.load(open(dataset_name, 'rb'))

    data = np.array(dataset['data'], dtype=int)
    attributes = dataset['attributes']
    # print(data[0])

    data, attributes = use_17_features(data, attributes)

    print(attributes)
    print(data.shape)
    assert(len(attributes) == data.shape[1])

    return data, attributes


def process_dataset(dataset, no_attributes):

    # Separate X and y
    x = dataset[:, 0:(no_attributes-1)]
    y = dataset[:, (no_attributes-1)]

    # Split into training and testing set.
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=TEST_SET_SIZE)

    return x_train, x_test, y_train, y_test


def learn_model(classifier, x_train, y_train, x_test, y_test, save_model=False):

    # Learn the model on training set
    classifier.fit(x_train, y_train)

    '''dot_data = export_graphviz(classifier, out_file="test.dot", feature_names=attributes[:-1],
                         filled=True, rounded=True,
                         special_characters=True)
    #graph = graphviz.Source(dot_data)
    #graph.render("test")'''

    if save_model:
        # Save the model to disk
        pickle.dump(classifier, open(MODEL_SAVE_FILE, 'wb'))

        # Fifty years later ...
        # clf = None

        # Load the model from disk
        # clf = pickle.load(open(MODEL_SAVE_FILE, 'rb'))

    # Test on testing set
    test_accuracy = classifier.score(x_test, y_test)
    print("Test accuracy: " + str(test_accuracy))


def learn_models(x_train, y_train, x_test, y_test):

    for model_name, model in models.iteritems():

        print("\nRunning %s..." % model_name)

        learn_model(model, x_train, y_train, x_test, y_test)


def main():

    if len(sys.argv) != 2:
        print("Error! Please provide an input file as the dataset.")
        sys.exit(1)

    dataset, attributes = load_dataset(sys.argv[1])
    no_attributes = len(attributes)
    no_examples = len(dataset)
    print(no_examples)

    x_train, x_test, y_train, y_test = process_dataset(dataset, no_attributes)

    learn_models(x_train, y_train, x_test, y_test)


if __name__ == '__main__':
    main()
